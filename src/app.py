from typing import Annotated

from aioinject import Inject
from aioinject.ext.fastapi import InjectMiddleware, inject
from common.web.exceptions import APIException, api_exception_handler
from fastapi import FastAPI
from sqlalchemy.ext.asyncio import AsyncSession
from starlette.middleware.cors import CORSMiddleware

from di import create_container
from gql.app import create_gql_app


def create_app() -> FastAPI:
    app = FastAPI()
    app.mount("/graphql/", create_gql_app())

    app.add_exception_handler(
        APIException,
        handler=api_exception_handler,
    )

    app.add_middleware(
        CORSMiddleware,
        allow_origins="*",
        allow_methods="*",
    )
    app.add_middleware(
        InjectMiddleware,
        container=create_container(),
    )

    @app.get("/healthcheck")
    @inject
    async def healthcheck(session: Annotated[AsyncSession, Inject]) -> None:
        await session.execute("select 1")

    return app
