from uuid import UUID


def to_uuid(id: UUID | str | None) -> UUID | None:
    if not id:
        return None

    if isinstance(id, UUID):
        return id

    try:
        return UUID(id)
    except ValueError:
        return None
