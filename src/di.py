import functools

import aioinject

from core.chapters.services import MangaChapterService
from core.groups.commands import MangaGroupCreateCommand
from core.groups.services import GroupService
from core.info.services import MangaInfoService
from core.manga.services import MangaApprovalService, MangaSearchService, MangaService
from core.tags.services import MangaTagService
from db.dependencies import get_session


@functools.lru_cache
def create_container() -> aioinject.Container:
    container = aioinject.Container()
    container.register(aioinject.Callable(get_session))
    container.register(aioinject.Callable(MangaService))
    container.register(aioinject.Callable(MangaTagService))
    container.register(aioinject.Callable(MangaChapterService))
    container.register(aioinject.Callable(MangaSearchService))
    container.register(aioinject.Callable(MangaInfoService))
    container.register(aioinject.Callable(MangaApprovalService))
    container.register(aioinject.Callable(GroupService))

    container.register(aioinject.Callable(MangaGroupCreateCommand))

    return container
