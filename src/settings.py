from pydantic import BaseSettings


class DatabaseSettings(BaseSettings):
    class Config:
        env_prefix = "database_"

    driver: str = "postgresql+asyncpg"
    database: str = "database"
    username: str = "postgres"
    password: str = "password"
    host: str = "postgres"

    echo: bool = False

    @property
    def url(self) -> str:
        return f"{self.driver}://{self.username}:{self.password}@{self.host}/{self.database}"

    @property
    def alembic_url(self) -> str:
        return self.url.replace("+asyncpg", "")


class AuthSettings(BaseSettings):
    class Config:
        env_prefix = "auth_"

    jwt_public_key: str
    jwt_algorithm: str = "RS256"


class FaustSettings(BaseSettings):
    class Config:
        env_prefix = "faust_"

    kafka_host: str = "kafka-0.kafka.manga.svc.cluster.local"


db = DatabaseSettings()
