from typing import Any

import httpx
from httpx._types import HeaderTypes


class GraphQLClient:
    def __init__(
        self,
        http_client: httpx.AsyncClient,
        endpoint_path: str,
    ):
        self.http_client = http_client
        self.endpoint_path = endpoint_path

    async def __call__(
        self,
        query: str,
        variables: dict[str, Any] | None = None,
        assert_errors: bool = True,
        headers: HeaderTypes | None = None,
    ) -> dict[str, Any]:
        response = await self.http_client.post(
            self.endpoint_path,
            json={
                "query": query,
                "variables": variables,
            },
            headers=headers,
        )
        response_json = response.json()
        if assert_errors:
            assert not response_json.get("errors")
        return response_json  # type: ignore
