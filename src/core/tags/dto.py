import pydantic

from schema import BaseDto


class MangaTagCreateDto(BaseDto):
    name: str = pydantic.Field(max_length=40)
