import slugify
from sqlalchemy import or_, select
from sqlalchemy.ext.asyncio import AsyncSession

from core.exceptions import EntityAlreadyExists
from core.tags.dto import MangaTagCreateDto
from db.models import MangaTag


class MangaTagService:
    def __init__(
        self,
        session: AsyncSession,
    ):
        self._session = session

    async def create(self, dto: MangaTagCreateDto) -> MangaTag:
        tag = MangaTag(
            name=dto.name,
            name_slug=slugify.slugify(dto.name),
        )
        if await self._session.scalar(
            select(MangaTag).where(
                or_(
                    MangaTag.name == tag.name,
                    MangaTag.name_slug == tag.name_slug,
                )
            )
        ):
            raise EntityAlreadyExists

        self._session.add(tag)
        await self._session.flush()
        await self._session.refresh(tag)
        return tag

    async def list(self) -> list[MangaTag]:
        stmt = select(MangaTag).order_by(MangaTag.name_slug)
        tags: list[MangaTag] = (await self._session.scalars(stmt)).all()
        return tags
