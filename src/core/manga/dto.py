from pydantic import Field

from schema import BaseDto


class MangaCreateDto(BaseDto):
    title: str = Field(strip_whitespace=True, max_length=80)
