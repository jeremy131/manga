import uuid

from db.models._language import Language
from schema import BaseDto


class MangaInfoCreateDto(BaseDto):
    manga_id: uuid.UUID
    language: Language
    title: str
    description: str
