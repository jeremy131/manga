import uuid

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from core.info.dto import MangaInfoCreateDto
from db.models import MangaInfo


class MangaInfoService:
    def __init__(self, session: AsyncSession):
        self._session = session

    async def list(
        self,
        manga_ids: list[uuid.UUID],
    ) -> list[MangaInfo]:
        stmt = select(MangaInfo)
        if manga_ids:
            stmt = stmt.where(MangaInfo.manga_id.in_(manga_ids))

        infos: list[MangaInfo] = (await self._session.scalars(stmt)).all()
        return infos

    async def create(
        self,
        dto: MangaInfoCreateDto,
    ) -> MangaInfo:
        info = MangaInfo(**dto.dict())
        self._session.add(info)
        await self._session.flush()
        await self._session.refresh(info)
        return info
