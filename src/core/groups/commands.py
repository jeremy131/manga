import uuid

from common.auth.models import AuthTokenPayload

from db.models import MangaGroup

from ..exceptions import EntityAlreadyExists
from .dtos import MangaGroupCreateDto
from .services import GroupService


class MangaGroupCreateCommand:
    def __init__(
        self,
        group_service: GroupService,
    ):
        self._group_service = group_service

    async def execute(
        self,
        dto: MangaGroupCreateDto,
        auth: AuthTokenPayload,
    ) -> MangaGroup:
        if await self._group_service.get(title=dto.title):
            raise EntityAlreadyExists

        return await self._group_service.create(dto, created_by=uuid.UUID(auth.sub))
