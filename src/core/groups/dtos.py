from pydantic import Field

from schema import BaseDto


class MangaGroupCreateDto(BaseDto):
    title: str = Field(min_length=1, max_length=200)
