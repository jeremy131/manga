from uuid import UUID

from sqlalchemy import nullslast, select
from sqlalchemy.ext.asyncio import AsyncSession

from db.models import MangaGroup, MangaGroupMembership, User

from .dtos import MangaGroupCreateDto


class GroupService:
    def __init__(
        self,
        session: AsyncSession,
    ) -> None:
        self._session = session

    async def get(
        self,
        id: UUID | None = None,
        title: str | None = None,
    ) -> MangaGroup | None:
        stmt = select(MangaGroup)
        if id:
            stmt = stmt.where(MangaGroup.id == id)
        if title:
            stmt = stmt.where(MangaGroup.title == title)

        if stmt.whereclause is None:
            return None

        group: MangaGroup | None = await self._session.scalar(stmt)
        return group

    async def create(
        self,
        dto: MangaGroupCreateDto,
        created_by: UUID,
    ) -> MangaGroup:
        group = MangaGroup(
            title=dto.title,
            created_by_id=created_by,
        )
        self._session.add(group)
        await self._session.flush()
        await self._session.refresh(group)
        return group

    async def group_memberships(self, group_id: UUID) -> list[MangaGroupMembership]:
        stmt = (
            select(MangaGroupMembership)
            .join(User, User.id == MangaGroupMembership.member_id, isouter=True)
            .where(MangaGroupMembership.group_id == group_id)
            .order_by(
                nullslast(User.username),
                MangaGroupMembership.member_id,
            )
        )
        members: list[MangaGroupMembership] = (await self._session.scalars(stmt)).all()
        return members
