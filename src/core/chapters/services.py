import uuid

from sqlalchemy import ARRAY, Integer, cast, func, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.sql import Select

from db.models import Language, Manga, MangaBranch, MangaChapter
from utils import to_uuid


class MangaChapterService:
    def __init__(
        self,
        session: AsyncSession,
    ):
        self._session = session

    async def get(
        self,
        id: str | uuid.UUID,
    ) -> MangaChapter | None:
        if not (chapter_id := to_uuid(id)):
            return None

        chapter: MangaChapter | None = await self._session.get(
            MangaChapter, ident=chapter_id
        )
        return chapter

    async def recent(
        self,
        languages: list[Language] | None = None,
    ) -> list[MangaChapter]:
        stmt = (
            select(MangaChapter)
            .order_by(MangaChapter.created_at.desc())
            .join(MangaChapter.branch)
        )
        if languages:
            stmt = stmt.where(MangaBranch.language.in_(languages))

        chapters: list[MangaChapter] = (await self._session.scalars(stmt)).all()
        return chapters

    @staticmethod
    def manga_chapters_query(manga_id: uuid.UUID) -> Select:
        return (
            select(MangaChapter)
            .join(MangaChapter.branch)
            .join(MangaBranch.manga)
            .where(Manga.id == manga_id)
            .order_by(
                cast(
                    func.string_to_array(MangaChapter.number, "."), ARRAY(Integer)
                ).desc()
            )
        )
