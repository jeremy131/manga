from sqlalchemy import Column, String
from sqlalchemy.orm import Mapped, relationship

from db import Base

from ._info import MangaInfo
from ._mixins import HasPrivate, HasTimestamps, HasUUID
from ._tag import manga_manga_tag_secondary_table


class Manga(Base, HasUUID, HasPrivate, HasTimestamps):
    __tablename__ = "manga"

    title: Mapped[str] = Column(
        String(250),
        nullable=False,
        unique=True,
    )
    title_slug: Mapped[str] = Column(
        String(250),
        nullable=False,
        unique=True,
    )

    info: list[MangaInfo] = relationship("MangaInfo", back_populates="manga")
    tags = relationship(
        "MangaTag", secondary=manga_manga_tag_secondary_table, back_populates="manga"
    )
    branches = relationship("MangaBranch", back_populates="manga")
