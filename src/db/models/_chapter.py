from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import Mapped, relationship

from db import Base
from db.models._mixins import HasTimestamps, HasUUID


class MangaChapter(Base, HasUUID, HasTimestamps):
    __tablename__ = "manga_chapter"

    branch_id = Column(ForeignKey("manga_branch.id"), nullable=False, index=True)
    branch = relationship("MangaBranch", back_populates="chapters")

    created_by_id = Column(UUID(as_uuid=True), nullable=False)
    title = Column(String(250), nullable=False)

    volume: Mapped[int | None] = Column(Integer)
    number: Mapped[str] = Column(String(40), nullable=False)

    pages = relationship(
        "MangaPage", order_by="MangaPage.number", back_populates="chapter"
    )
