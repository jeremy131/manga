from sqlalchemy import Column, ForeignKey, String, Table
from sqlalchemy.orm import Mapped, relationship

from db import Base
from db.models._mixins import HasUUID

manga_manga_tag_secondary_table = Table(
    "manga__manga_tag__secondary",
    Base.metadata,
    Column("manga_id", ForeignKey("manga.id"), primary_key=True),
    Column("tag_id", ForeignKey("manga_tag.id"), primary_key=True),
)


class MangaTag(Base, HasUUID):
    __tablename__ = "manga_tag"

    name: Mapped[str] = Column(String(100), nullable=False, unique=True)
    name_slug: Mapped[str] = Column(String(100), nullable=False, unique=True)
    manga = relationship(
        "Manga", secondary=manga_manga_tag_secondary_table, back_populates="tags"
    )
