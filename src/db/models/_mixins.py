import datetime
import uuid

from sqlalchemy import Boolean, Column, DateTime, event, func, orm, true
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import Mapped, ORMExecuteState, Session
from sqlalchemy.sql import expression


class HasUUID:
    id: Mapped[uuid.UUID] = Column(
        UUID(as_uuid=True),
        default=uuid.uuid4,
        primary_key=True,
    )


class HasPrivate:
    is_public: Mapped[bool] = Column(
        Boolean,
        nullable=False,
        default=True,
        server_default=expression.true(),
    )


class HasTimestamps:
    created_at: Mapped[datetime.datetime] = Column(
        DateTime(timezone=True),
        nullable=False,
        server_default=func.now(),
    )
    updated_at: Mapped[datetime.datetime] = Column(
        DateTime(timezone=True),
        nullable=False,
        server_default=func.now(),
        server_onupdate=func.now(),
    )


@event.listens_for(Session, "do_orm_execute")
def _filter(state: ORMExecuteState) -> None:
    if (
        not state.is_column_load
        and not state.is_relationship_load
        and not state.execution_options.get("include_private", False)
    ):
        state.statement = state.statement.options(
            orm.with_loader_criteria(
                HasPrivate,
                lambda cls: cls.is_public == true(),
                include_aliases=True,
            )
        )
    return None
