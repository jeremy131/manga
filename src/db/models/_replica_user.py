import datetime
import uuid

from sqlalchemy import Column, DateTime, String
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import Mapped

from db import Base


class User(Base):
    __tablename__ = "_replica_user"

    id: Mapped[uuid.UUID] = Column(
        postgresql.UUID(as_uuid=True),
        primary_key=True,
    )
    username: str = Column(
        String(100),
        nullable=False,
        unique=True,
    )
    created_at: datetime.datetime = Column(
        DateTime(timezone=True),
        nullable=False,
    )
