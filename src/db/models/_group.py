import uuid

from sqlalchemy import Column, ForeignKey, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import Mapped, relationship

from db import Base

from ._mixins import HasTimestamps, HasUUID


class MangaGroup(Base, HasUUID, HasTimestamps):
    __tablename__ = "manga_group"

    created_by_id = Column(UUID(as_uuid=True), nullable=False)
    title: Mapped[str] = Column(
        String(250),
        nullable=False,
    )

    memberships = relationship("MangaGroupMembership", back_populates="group")
    branches = relationship("MangaBranch", back_populates="group")


class MangaGroupMembership(Base):
    __tablename__ = "manga_group_membership"

    group_id = Column(ForeignKey("manga_group.id"), primary_key=True)
    member_id: uuid.UUID = Column(UUID(as_uuid=True), primary_key=True)

    group = relationship("MangaGroup", back_populates="memberships")
