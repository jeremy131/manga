from sqlalchemy import Column, ForeignKey, Integer, String, UniqueConstraint
from sqlalchemy.orm import relationship

from db import Base
from db.models._mixins import HasUUID


class MangaPage(Base, HasUUID):
    __tablename__ = "manga_page"

    chapter_id = Column(ForeignKey("manga_chapter.id"), nullable=False, index=True)
    chapter = relationship("MangaChapter", back_populates="pages")
    number = Column(Integer, nullable=False)
    image_url = Column(String(250), nullable=False)

    __table_args__ = (UniqueConstraint("chapter_id", "number"),)
