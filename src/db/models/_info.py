import uuid

from sqlalchemy import Column, Computed, Enum, ForeignKey, Index, String
from sqlalchemy.dialects.postgresql import TSVECTOR
from sqlalchemy.engine.default import DefaultExecutionContext
from sqlalchemy.orm import Mapped, relationship

from db import Base
from db.types import RegConfig

from ._language import Language, RegConfigLanguage
from ._mixins import HasUUID


def _regconfig_default(ctx: DefaultExecutionContext) -> str:
    return RegConfigLanguage[ctx.current_parameters["language"].value].value


class MangaInfo(Base, HasUUID):
    __tablename__ = "manga_info"

    manga_id: Mapped[uuid.UUID] = Column(
        ForeignKey("manga.id"),
        nullable=False,
        index=True,
    )
    manga = relationship("Manga", back_populates="info")
    language: Language = Column(
        Enum(Language, native_enum=False),
        nullable=False,
    )
    language_regconfig = Column(
        RegConfig,
        nullable=False,
        default=_regconfig_default,
    )
    title: str = Column(
        String(250),
        nullable=False,
    )
    description: str = Column(
        String(1000),
        nullable=False,
    )
    search_ts_vector = Column(
        TSVECTOR,
        Computed(
            "setweight(to_tsvector(language_regconfig, coalesce(title, '')), 'A') || "
            "setweight(to_tsvector(language_regconfig, coalesce(description, '')), 'D')",
            persisted=True,
        ),
        nullable=False,
    )

    __table_args__ = (
        Index(
            "ix_manga_info_search_ts_vector", search_ts_vector, postgresql_using="gin"
        ),
    )
