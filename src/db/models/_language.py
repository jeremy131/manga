import enum


class RegConfigLanguage(enum.Enum):
    eng = "english"
    rus = "russian"
    ukr = "ukrainian"


class Language(enum.Enum):
    eng = "eng"
    rus = "rus"
    ukr = "ukr"
    deu = "deu"
