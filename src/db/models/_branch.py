import uuid

from sqlalchemy import Column, Enum, ForeignKey
from sqlalchemy.orm import Mapped, relationship

from db import Base

from ._language import Language
from ._mixins import HasTimestamps, HasUUID


class MangaBranch(Base, HasUUID, HasTimestamps):
    __tablename__ = "manga_branch"

    manga_id: Mapped[uuid.UUID] = Column(
        ForeignKey("manga.id"),
        nullable=False,
    )
    group_id: Mapped[uuid.UUID] = Column(
        ForeignKey("manga_group.id"),
        nullable=False,
    )
    language = Column(
        Enum(Language, native_enum=False),
        nullable=False,
    )
    chapters = relationship("MangaChapter", back_populates="branch")
    manga = relationship("Manga", back_populates="branches")
    group = relationship("MangaGroup", back_populates="branches")
