from typing import Any

from sqlalchemy.sql.type_api import UserDefinedType


class RegConfig(UserDefinedType):
    cache_ok = True

    def get_col_spec(self, **kw: Any) -> str:
        return "regconfig"  # pragma: no cover
