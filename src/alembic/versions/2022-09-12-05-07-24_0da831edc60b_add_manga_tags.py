"""Add Manga Tags

Revision ID: 0da831edc60b
Revises: 48cd8df0b30a
Create Date: 2022-09-12 05:07:24.141508

"""
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

from alembic import op

# revision identifiers, used by Alembic.
revision = "0da831edc60b"
down_revision = "48cd8df0b30a"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "manga_tag",
        sa.Column("id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column("name", sa.String(length=100), nullable=False),
        sa.Column("name_slug", sa.String(length=100), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_manga_tag")),
        sa.UniqueConstraint("name", name=op.f("uq_manga_tag_name")),
        sa.UniqueConstraint("name_slug", name=op.f("uq_manga_tag_name_slug")),
    )
    op.create_table(
        "manga__manga_tag__secondary",
        sa.Column("manga_id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column("tag_id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.ForeignKeyConstraint(
            ["manga_id"],
            ["manga.id"],
            name=op.f("fk_manga__manga_tag__secondary_manga_id_manga"),
        ),
        sa.ForeignKeyConstraint(
            ["tag_id"],
            ["manga_tag.id"],
            name=op.f("fk_manga__manga_tag__secondary_tag_id_manga_tag"),
        ),
        sa.PrimaryKeyConstraint(
            "manga_id", "tag_id", name=op.f("pk_manga__manga_tag__secondary")
        ),
    )


def downgrade():
    op.drop_table("manga__manga_tag__secondary")
    op.drop_table("manga_tag")
