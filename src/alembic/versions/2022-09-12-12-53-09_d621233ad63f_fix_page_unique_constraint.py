"""Fix page unique constraint

Revision ID: d621233ad63f
Revises: 0da831edc60b
Create Date: 2022-09-12 12:53:09.686352

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "d621233ad63f"
down_revision = "0da831edc60b"
branch_labels = None
depends_on = None


def upgrade():
    op.drop_constraint("uq_manga_page_id", "manga_page", type_="unique")
    op.create_unique_constraint(
        op.f("uq_manga_page_chapter_id"), "manga_page", ["chapter_id", "number"]
    )


def downgrade():
    op.drop_constraint(op.f("uq_manga_page_chapter_id"), "manga_page", type_="unique")
    op.create_unique_constraint("uq_manga_page_id", "manga_page", ["id", "number"])
