"""Initial

Revision ID: 48cd8df0b30a
Revises:
Create Date: 2022-09-04 10:03:17.624838

"""
from typing import Any

import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
from sqlalchemy.sql.type_api import UserDefinedType

from alembic import op

revision = "48cd8df0b30a"
down_revision = None
branch_labels = None
depends_on = None


class RegConfig(UserDefinedType):
    cache_ok = True

    def get_col_spec(self, **kw: Any) -> str:
        return "regconfig"


def upgrade():
    op.create_table(
        "manga",
        sa.Column("id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column(
            "is_public", sa.Boolean(), server_default=sa.text("true"), nullable=False
        ),
        sa.Column(
            "created_at",
            sa.DateTime(timezone=True),
            server_default=sa.text("now()"),
            nullable=False,
        ),
        sa.Column(
            "updated_at",
            sa.DateTime(timezone=True),
            server_default=sa.text("now()"),
            nullable=False,
        ),
        sa.Column("title", sa.String(length=250), nullable=False),
        sa.Column("title_slug", sa.String(length=250), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_manga")),
        sa.UniqueConstraint("title", name=op.f("uq_manga_title")),
        sa.UniqueConstraint("title_slug", name=op.f("uq_manga_title_slug")),
    )
    op.create_table(
        "manga_group",
        sa.Column("id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column(
            "created_at",
            sa.DateTime(timezone=True),
            server_default=sa.text("now()"),
            nullable=False,
        ),
        sa.Column(
            "updated_at",
            sa.DateTime(timezone=True),
            server_default=sa.text("now()"),
            nullable=False,
        ),
        sa.Column("created_by_id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column("title", sa.String(length=250), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_manga_group")),
    )
    op.create_table(
        "manga_branch",
        sa.Column("id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column(
            "created_at",
            sa.DateTime(timezone=True),
            server_default=sa.text("now()"),
            nullable=False,
        ),
        sa.Column(
            "updated_at",
            sa.DateTime(timezone=True),
            server_default=sa.text("now()"),
            nullable=False,
        ),
        sa.Column("manga_id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column("group_id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column(
            "language",
            sa.Enum("eng", name="language", native_enum=False),
            nullable=False,
        ),
        sa.ForeignKeyConstraint(
            ["group_id"],
            ["manga_group.id"],
            name=op.f("fk_manga_branch_group_id_manga_group"),
        ),
        sa.ForeignKeyConstraint(
            ["manga_id"], ["manga.id"], name=op.f("fk_manga_branch_manga_id_manga")
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_manga_branch")),
    )
    op.create_table(
        "manga_group_membership",
        sa.Column("group_id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column("member_id", postgresql.UUID(), nullable=False),
        sa.ForeignKeyConstraint(
            ["group_id"],
            ["manga_group.id"],
            name=op.f("fk_manga_group_membership_group_id_manga_group"),
        ),
        sa.PrimaryKeyConstraint(
            "group_id", "member_id", name=op.f("pk_manga_group_membership")
        ),
    )
    op.create_table(
        "manga_info",
        sa.Column("id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column("manga_id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column(
            "language",
            sa.Enum("eng", name="language", native_enum=False),
            nullable=False,
        ),
        sa.Column("language_regconfig", RegConfig(), nullable=False),
        sa.Column("title", sa.String(length=250), nullable=False),
        sa.Column("description", sa.String(length=1000), nullable=False),
        sa.Column(
            "search_ts_vector",
            postgresql.TSVECTOR(),
            sa.Computed(
                "setweight(to_tsvector(language_regconfig, coalesce(title, '')), 'A') || setweight(to_tsvector(language_regconfig, coalesce(description, '')), 'D')",
                persisted=True,
            ),
            nullable=False,
        ),
        sa.ForeignKeyConstraint(
            ["manga_id"], ["manga.id"], name=op.f("fk_manga_info_manga_id_manga")
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_manga_info")),
    )
    op.create_index(
        op.f("ix_manga_info_manga_id"), "manga_info", ["manga_id"], unique=False
    )
    op.create_index(
        "ix_manga_info_search_ts_vector",
        "manga_info",
        ["search_ts_vector"],
        unique=False,
        postgresql_using="gin",
    )
    op.create_table(
        "manga_chapter",
        sa.Column("id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column(
            "created_at",
            sa.DateTime(timezone=True),
            server_default=sa.text("now()"),
            nullable=False,
        ),
        sa.Column(
            "updated_at",
            sa.DateTime(timezone=True),
            server_default=sa.text("now()"),
            nullable=False,
        ),
        sa.Column("branch_id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column("created_by_id", postgresql.UUID(), nullable=False),
        sa.Column("title", sa.String(length=250), nullable=False),
        sa.Column("volume", sa.Integer(), nullable=True),
        sa.Column("number", sa.String(length=40), nullable=False),
        sa.ForeignKeyConstraint(
            ["branch_id"],
            ["manga_branch.id"],
            name=op.f("fk_manga_chapter_branch_id_manga_branch"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_manga_chapter")),
    )
    op.create_index(
        op.f("ix_manga_chapter_branch_id"), "manga_chapter", ["branch_id"], unique=False
    )
    op.create_table(
        "manga_page",
        sa.Column("id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column("chapter_id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column("number", sa.Integer(), nullable=False),
        sa.Column("image_url", sa.String(length=250), nullable=False),
        sa.ForeignKeyConstraint(
            ["chapter_id"],
            ["manga_chapter.id"],
            name=op.f("fk_manga_page_chapter_id_manga_chapter"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_manga_page")),
        sa.UniqueConstraint("id", "number", name=op.f("uq_manga_page_id")),
    )
    op.create_index(
        op.f("ix_manga_page_chapter_id"), "manga_page", ["chapter_id"], unique=False
    )


def downgrade():
    op.drop_index(op.f("ix_manga_page_chapter_id"), table_name="manga_page")
    op.drop_table("manga_page")
    op.drop_index(op.f("ix_manga_chapter_branch_id"), table_name="manga_chapter")
    op.drop_table("manga_chapter")
    op.drop_index(
        "ix_manga_info_search_ts_vector",
        table_name="manga_info",
        postgresql_using="gin",
    )
    op.drop_index(op.f("ix_manga_info_manga_id"), table_name="manga_info")
    op.drop_table("manga_info")
    op.drop_table("manga_group_membership")
    op.drop_table("manga_branch")
    op.drop_table("manga_group")
    op.drop_table("manga")
