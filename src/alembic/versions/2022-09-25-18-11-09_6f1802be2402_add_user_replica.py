"""Add user replica

Revision ID: 6f1802be2402
Revises: d621233ad63f
Create Date: 2022-09-25 18:11:09.073114

"""
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

from alembic import op

# revision identifiers, used by Alembic.
revision = "6f1802be2402"
down_revision = "d621233ad63f"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "_replica_user",
        sa.Column("id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column("username", sa.String(length=100), nullable=False),
        sa.Column("created_at", sa.DateTime(timezone=True), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk__replica_user")),
        sa.UniqueConstraint("username", name=op.f("uq__replica_user_username")),
    )


def downgrade():
    op.drop_table("_replica_user")
