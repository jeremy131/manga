import enum

import strawberry


@strawberry.federation.enum(name="Language")
class LanguageType(enum.Enum):
    en = "eng"
    ru = "rus"
    uk = "ukr"
    de = "deu"
