from typing import Any, Type, TypeVar

import pydantic
import strawberry
from pydantic import BaseModel

from gql.errors import ErrorType

TBaseModel = TypeVar("TBaseModel", bound=BaseModel)


@strawberry.federation.type(shareable=True)
class ValidationError(ErrorType):
    code: str
    message: str
    location: list[str]


@strawberry.federation.type(shareable=True)
class ValidationErrors(ErrorType):
    message: str = "Validation Error"
    errors: list[ValidationError] = strawberry.field(default_factory=list)


def validate(
    input: Any, dto: Type[TBaseModel]
) -> tuple[TBaseModel, ValidationErrors | None]:
    if not hasattr(input, "_type_definition"):
        raise ValueError
    try:
        return dto.from_orm(input), None
    except pydantic.ValidationError as exc:
        errors = [
            ValidationError(
                location=list(map(str, error["loc"])),
                message=error["msg"],
                code=error["type"],
            )
            for error in exc.errors()
        ]
        return None, ValidationErrors(errors=errors)  # type: ignore
