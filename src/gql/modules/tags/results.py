import strawberry.federation

from gql.validation import ValidationErrors

from ...errors import EntityAlreadyExistsError
from .types import MangaTagType

MangaTagCreateErrors = strawberry.union(
    name="MangaTagCreateErrors",
    types=(
        ValidationErrors,
        EntityAlreadyExistsError,
    ),
)


@strawberry.type
class MangaTagCreatePayload:
    tag: MangaTagType | None = None
    errors: list[MangaTagCreateErrors] = strawberry.field(default_factory=list)
