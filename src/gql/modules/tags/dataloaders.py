import collections
from uuid import UUID

from common.utils import filter_uuids
from sqlalchemy import select

from db.base import async_sessionmaker
from db.models import MangaTag, manga_manga_tag_secondary_table


async def load_manga_tags(manga_ids: list[UUID | str]) -> list[list[MangaTag]]:
    stmt = (
        select(MangaTag, manga_manga_tag_secondary_table.c.manga_id)
        .join(
            manga_manga_tag_secondary_table,
            manga_manga_tag_secondary_table.c.tag_id == MangaTag.id,
        )
        .where(manga_manga_tag_secondary_table.c.manga_id.in_(filter_uuids(manga_ids)))
        .order_by(MangaTag.name_slug)
    )

    async with async_sessionmaker() as session:
        result = await session.execute(stmt)

    manga_tags: dict[str, list[MangaTag]] = collections.defaultdict(list)

    for tag, manga_id in result:
        manga_tags[str(manga_id)].append(tag)

    return [manga_tags[str(id)] for id in manga_ids]
