import strawberry.federation
from common.utils import OrmMixin

from db.models import MangaTag


@strawberry.federation.type(name="MangaTag")
class MangaTagType(OrmMixin[MangaTag, "MangaTagType"]):
    id: strawberry.ID
    name: str
    name_slug: str

    @classmethod
    def from_orm(cls, model: MangaTag) -> "MangaTagType":
        return MangaTagType(
            id=strawberry.ID(str(model.id)),
            name=model.name,
            name_slug=model.name_slug,
        )
