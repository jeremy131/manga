from typing import Annotated

import strawberry
from aioinject import Inject
from aioinject.ext.strawberry import inject

from core.tags.services import MangaTagService

from .types import MangaTagType


@strawberry.type
class MangaTagQuery:
    @strawberry.field
    @inject
    async def manga_tags(
        self,
        tag_service: Annotated[MangaTagService, Inject],
    ) -> list[MangaTagType]:
        tags = await tag_service.list()
        return MangaTagType.from_orm_list(tags)
