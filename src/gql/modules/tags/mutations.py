from typing import Annotated

import strawberry.federation
from aioinject import Inject
from aioinject.ext.strawberry import inject

from core.exceptions import EntityAlreadyExists
from core.tags.dto import MangaTagCreateDto
from core.tags.services import MangaTagService
from gql.validation import validate

from ...errors import EntityAlreadyExistsError
from .inputs import MangaTagCreateInput
from .results import MangaTagCreatePayload
from .types import MangaTagType


@strawberry.federation.type
class MangaTagMutation:
    @strawberry.mutation
    @inject
    async def manga_tag_create(
        self,
        input: MangaTagCreateInput,
        tag_service: Annotated[MangaTagService, Inject],
    ) -> MangaTagCreatePayload:
        dto, errors = validate(input, MangaTagCreateDto)
        if errors:
            return MangaTagCreatePayload(errors=[errors])

        try:
            tag = await tag_service.create(dto)
        except EntityAlreadyExists:
            return MangaTagCreatePayload(errors=[EntityAlreadyExistsError()])

        return MangaTagCreatePayload(tag=MangaTagType.from_orm(tag))
