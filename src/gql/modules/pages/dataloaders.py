import collections
from typing import Iterable
from uuid import UUID

from common.utils import filter_uuids
from sqlalchemy import select

from db.base import async_sessionmaker
from db.models import MangaPage


async def load_chapter_pages(chapter_ids: list[UUID | str]) -> list[list[MangaPage]]:
    stmt = (
        select(MangaPage)
        .where(MangaPage.chapter_id.in_(filter_uuids(chapter_ids)))
        .order_by(MangaPage.number.asc())
    )
    async with async_sessionmaker() as session:
        pages: Iterable[MangaPage] = await session.scalars(stmt)

    pages_by_chapter_id = collections.defaultdict(list)
    for page in pages:
        pages_by_chapter_id[str(page.chapter_id)].append(page)
    return [pages_by_chapter_id[str(chapter_id)] for chapter_id in chapter_ids]
