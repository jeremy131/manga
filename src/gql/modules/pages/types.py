import strawberry.federation
from common.utils import OrmMixin

from db.models import MangaPage


@strawberry.federation.type(
    name="MangaPage",
    keys=["id"],
)
class MangaPageType(OrmMixin[MangaPage, "MangaPageType"]):
    id: strawberry.ID
    number: int
    image_url: str

    @classmethod
    def from_orm(cls, model: MangaPage) -> "MangaPageType":
        return MangaPageType(
            id=strawberry.ID(str(model.id)),
            number=model.number,
            image_url=model.image_url,
        )
