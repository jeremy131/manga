import strawberry


@strawberry.input
class TagsSearchInput:
    include: list[str] | None = None
    exclude: list[str] | None = None


@strawberry.input
class MangaSearchInput:
    search_term: str | None = None
    tags_include: list[str] | None = None
    tags_exclude: list[str] | None = None


@strawberry.federation.input(name="MangaCreateInput")
class MangaCreateInput:
    title: str
