import collections
from typing import Iterable
from uuid import UUID

from common.utils import filter_uuids
from sqlalchemy import select

from db.base import async_sessionmaker
from db.models import Manga, MangaBranch, MangaInfo


async def load_manga_by_id(manga_ids: list[UUID | str]) -> list[Manga | None]:
    stmt = select(Manga).where(Manga.id.in_(filter_uuids(manga_ids)))

    async with async_sessionmaker() as session:
        manga_by_id = {str(manga.id): manga for manga in await session.scalars(stmt)}

    return [manga_by_id.get(str(id)) for id in manga_ids]


async def load_manga_infos_by_manga_id(
    manga_ids: list[UUID | str],
) -> list[list[MangaInfo]]:
    stmt = (
        select(MangaInfo)
        .where(MangaInfo.manga_id.in_(filter_uuids(manga_ids)))
        .order_by(MangaInfo.id)
    )

    async with async_sessionmaker() as session:
        infos: Iterable[MangaInfo] = await session.scalars(stmt)

    infos_by_manga_id = collections.defaultdict(list)
    for info in infos:
        infos_by_manga_id[str(info.manga_id)].append(info)

    return [infos_by_manga_id[str(id)] for id in manga_ids]


async def load_manga_by_branch_id(
    branch_ids: list[UUID],
) -> list[Manga | None]:
    stmt = (
        select(Manga, MangaBranch.id)
        .join(Manga.branches)
        .where(MangaBranch.id.in_(branch_ids))
    )

    async with async_sessionmaker() as session:
        result = (await session.execute(stmt)).all()
    manga_by_branch_id = {branch_id: manga for manga, branch_id in result}
    return [manga_by_branch_id.get(id) for id in branch_ids]
