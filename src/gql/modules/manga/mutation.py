from typing import Annotated

import strawberry
from aioinject import Inject
from aioinject.ext.strawberry import inject

from core.manga.dto import MangaCreateDto
from core.manga.services import MangaService
from gql.validation import validate

from .inputs import MangaCreateInput
from .results import MangaCreatePayload
from .types import MangaType


@strawberry.type
class MangaMutation:
    @strawberry.mutation
    @inject
    async def manga_create(
        self,
        input: MangaCreateInput,
        manga_service: Annotated[MangaService, Inject],
    ) -> MangaCreatePayload:
        dto, errors = validate(input, MangaCreateDto)
        if errors:
            return MangaCreatePayload(errors=[errors])

        manga = await manga_service.create(dto)
        return MangaCreatePayload(manga=MangaType.from_orm(manga))
