import strawberry

from gql.validation import ValidationErrors

from .types import MangaType

MangaCreateErrors = strawberry.union("MangaCreateErrors", (ValidationErrors,))


@strawberry.federation.type(name="MangaCreatePayload")
class MangaCreatePayload:
    manga: MangaType | None = None
    errors: list[MangaCreateErrors] = strawberry.field(default_factory=list)
