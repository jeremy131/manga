from datetime import datetime

import strawberry
from common.utils import OrmMixin
from strawberry import Private

from core.chapters.services import MangaChapterService
from db.base import async_sessionmaker
from db.models import Language, Manga, MangaInfo
from gql.context import Info
from gql.modules.chapters.types import MangaChapterType
from gql.modules.tags.types import MangaTagType
from gql.pagination import PagePaginationResult, page_paginate
from gql.types import LanguageType


@strawberry.federation.type(
    keys=["id"],
    name="MangaInfo",
)
class MangaInfoType(OrmMixin[MangaInfo, "MangaInfoType"]):
    id: strawberry.ID
    language: LanguageType
    title: str
    description: str

    @classmethod
    def from_orm(cls, model: MangaInfo) -> "MangaInfoType":
        return MangaInfoType(
            id=strawberry.ID(str(model.id)),
            language=LanguageType(model.language.value),
            title=model.title,
            description=model.description,
        )


@strawberry.federation.type(
    keys=["id"],
    name="Manga",
)
class MangaType(OrmMixin[Manga, "MangaType"]):
    _instance: Private[Manga]

    id: strawberry.ID
    title: str
    title_slug: str
    created_at: datetime
    updated_at: datetime

    @classmethod
    def from_orm(cls, model: Manga) -> "MangaType":
        return MangaType(
            _instance=model,
            id=strawberry.ID(str(model.id)),
            title=model.title,
            title_slug=model.title_slug,
            created_at=model.created_at,
            updated_at=model.updated_at,
        )

    @strawberry.field
    async def infos(
        self,
        info: Info,
        preferred_languages: list[LanguageType] | None = None,
    ) -> list[MangaInfoType]:
        preferred_languages = preferred_languages or []
        preferred_languages = [Language[lang.value] for lang in preferred_languages]
        infos = await info.context.loaders.manga_info_by_manga_id.load(self.id)
        if preferred_languages:
            preferred_infos = [
                info for info in infos if info.language in preferred_languages
            ]
            preferred_infos.sort(
                key=lambda i: preferred_languages.index(i.language),
            )
            infos = preferred_infos or infos

        return MangaInfoType.from_orm_list(infos)

    @strawberry.field
    async def tags(
        self,
        info: Info,
    ) -> list[MangaTagType]:
        tags = await info.context.loaders.manga_tags.load(self.id)
        return MangaTagType.from_orm_list(tags)

    @strawberry.field
    async def chapters(
        self,
        page: int = 1,
        page_size: int = 100,
    ) -> PagePaginationResult[MangaChapterType]:
        stmt = MangaChapterService.manga_chapters_query(self._instance.id)
        async with async_sessionmaker() as session:
            return await page_paginate(
                query=stmt,
                page=page,
                page_size=page_size,
                session=session,
                type_=MangaChapterType,
            )
