from typing import Annotated

import strawberry
from aioinject import Inject
from aioinject.ext.strawberry import inject
from sqlalchemy.ext.asyncio import AsyncSession

from core.manga.services import MangaSearchService, MangaService
from gql.pagination import PagePaginationInput, PagePaginationResult, page_paginate
from utils import to_uuid

from .inputs import MangaSearchInput
from .types import MangaType


@strawberry.type
class MangaQuery:
    @strawberry.field
    @inject
    async def manga_get(
        self,
        service: Annotated[MangaService, Inject],
        id: strawberry.ID | None = None,
        title_slug: str | None = None,
    ) -> MangaType | None:
        manga = await service.get(
            entity_id=to_uuid(id),
            title_slug=title_slug,
        )
        return MangaType.from_orm_optional(manga)

    @strawberry.field
    @inject
    async def manga_search(
        self,
        *,
        pagination: PagePaginationInput,
        input: MangaSearchInput,
        session: Annotated[AsyncSession, Inject],
        search_service: Annotated[MangaSearchService, Inject],
    ) -> PagePaginationResult[MangaType]:
        pagination.to_pydantic()
        stmt = search_service.search_query(
            search_term=input.search_term,
            tags_include=input.tags_include,
            tags_exclude=input.tags_exclude,
        )
        return await page_paginate(
            stmt,
            page=pagination.page,
            page_size=pagination.page_size,
            session=session,
            type_=MangaType,
        )
