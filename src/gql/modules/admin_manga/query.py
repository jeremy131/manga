from typing import Annotated

import strawberry
from aioinject import Inject
from aioinject.ext.strawberry import inject

from core.manga.services import MangaApprovalService
from gql.context import Info
from gql.errors import PermissionErrorType

from ..manga.types import MangaType
from .results import MangaGetApprovalQueueResult


@strawberry.type
class AdminMangaQuery:
    @strawberry.field
    @inject
    async def manga_get_approval_queue(
        self,
        info: Info,
        service: Annotated[MangaApprovalService, Inject],
    ) -> MangaGetApprovalQueueResult:
        if (
            not (user := info.context.maybe_user)
            or "manga_approval:basic" not in user.permissions
        ):
            return MangaGetApprovalQueueResult(
                result=None, errors=[PermissionErrorType(message="Not Authorized")]
            )

        return MangaGetApprovalQueueResult(
            result=MangaType.from_orm_list(await service.get_all()),
        )
