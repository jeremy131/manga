import strawberry

from gql.errors import ErrorType
from gql.modules.manga.types import MangaType


@strawberry.federation.type
class MangaGetApprovalQueueResult:
    result: list[MangaType] | None = None
    errors: list[ErrorType] | None = None
