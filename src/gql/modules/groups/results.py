import strawberry

from gql.errors import EntityAlreadyExistsError, UnauthenticatedError
from gql.validation import ValidationErrors

from .types import MangaGroupType

MangaGroupCreateErrors = strawberry.union(
    name="MangaGroupCreateErrors",
    types=(
        UnauthenticatedError,
        ValidationErrors,
        EntityAlreadyExistsError,
    ),
)


@strawberry.type
class MangaGroupCreateResult:
    result: MangaGroupType | None = None
    errors: list[MangaGroupCreateErrors] = strawberry.field(default_factory=list)
