from typing import Annotated

import strawberry
from aioinject import Inject
from aioinject.ext.strawberry import inject

from core.exceptions import EntityAlreadyExists
from core.groups.commands import MangaGroupCreateCommand
from core.groups.dtos import MangaGroupCreateDto
from gql.validation import validate

from ...context import Info
from ...errors import EntityAlreadyExistsError, UnauthenticatedError
from .input import MangaGroupCreateInput
from .results import MangaGroupCreateResult
from .types import MangaGroupType


@strawberry.type
class MangaGroupMutation:
    @strawberry.mutation
    @inject
    async def manga_group_create(
        self,
        input: MangaGroupCreateInput,
        info: Info,
        command: Annotated[MangaGroupCreateCommand, Inject],
    ) -> MangaGroupCreateResult:
        user = info.context.maybe_user
        if not user:
            return MangaGroupCreateResult(errors=[UnauthenticatedError()])

        dto, errors = validate(input=input, dto=MangaGroupCreateDto)
        if errors:
            return MangaGroupCreateResult(errors=[errors])

        try:
            group = await command.execute(dto, auth=user)
        except EntityAlreadyExists:
            return MangaGroupCreateResult(errors=[EntityAlreadyExistsError()])

        return MangaGroupCreateResult(
            result=MangaGroupType.from_orm(group),
        )
