import datetime
from typing import Annotated

import strawberry
from aioinject import Inject
from aioinject.ext.strawberry import inject
from common.utils import OrmMixin
from strawberry import Private

from core.groups.services import GroupService
from db.models import MangaGroup
from gql.external_types import UserType


@strawberry.federation.type(
    name="MangaGroup",
    keys=["id"],
)
class MangaGroupType(OrmMixin[MangaGroup, "MangaGroupType"]):
    _instance: Private[MangaGroup]
    id: strawberry.ID
    title: str
    created_at: datetime.datetime

    @classmethod
    def from_orm(cls, model: MangaGroup) -> "MangaGroupType":
        return MangaGroupType(
            _instance=model,
            id=strawberry.ID(str(model.id)),
            title=model.title,
            created_at=model.created_at,
        )

    @strawberry.field
    async def created_by(self) -> UserType:
        return UserType(
            id=strawberry.ID(str(self._instance.created_by_id)),
        )

    @strawberry.field
    @inject
    async def members(
        self,
        service: Annotated[GroupService, Inject],
    ) -> list[UserType]:
        memberships = await service.group_memberships(self._instance.id)
        return [
            UserType(id=strawberry.ID(str(member.member_id))) for member in memberships
        ]
