from typing import Annotated

import strawberry
from aioinject import Inject
from aioinject.ext.strawberry import inject

from core.groups.services import GroupService
from gql.modules.groups.types import MangaGroupType
from utils import to_uuid


@strawberry.type
class MangaGroupQuery:
    @strawberry.field
    @inject
    async def manga_group_get(
        self,
        id: strawberry.ID,
        service: Annotated[GroupService, Inject],
    ) -> MangaGroupType | None:
        id = to_uuid(id)
        if not id:
            return None

        return MangaGroupType.from_orm_optional(await service.get(id))
