import strawberry
from strawberry.file_uploads import Upload

from .inputs import ChapterCreateInput
from .types import MangaChapterType


@strawberry.type
class MangaChapterMutation:
    @strawberry.mutation
    async def manga_chapter_create(
        self,
        input: ChapterCreateInput,
        images: list[Upload],
    ) -> MangaChapterType | None:
        raise NotImplementedError
