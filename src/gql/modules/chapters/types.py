from typing import TYPE_CHECKING, Annotated, cast

import strawberry.federation
from common.utils import OrmMixin
from strawberry import Private

from db.models import Manga, MangaChapter
from gql.context import Info
from gql.external_types import UserType
from gql.modules.pages.types import MangaPageType

if TYPE_CHECKING:
    from gql.modules.manga.types import MangaType


@strawberry.federation.type(
    name="MangaChapter",
    keys=["id"],
)
class MangaChapterType(OrmMixin[MangaChapter, "MangaChapterType"]):
    _instance: Private[MangaChapter]

    id: strawberry.ID
    title: str
    volume: int | None
    number: str

    @classmethod
    def from_orm(cls, model: MangaChapter) -> "MangaChapterType":
        return MangaChapterType(
            _instance=model,
            id=strawberry.ID(str(model.id)),
            title=model.title,
            volume=model.volume,
            number=model.number,
        )

    @strawberry.field
    async def created_by(self) -> UserType:
        return UserType(id=strawberry.ID(str(self._instance.created_by_id)))

    @strawberry.field
    async def pages(self, info: Info) -> list[MangaPageType]:
        pages = await info.context.loaders.pages_by_chapter_id.load(self._instance.id)
        return MangaPageType.from_orm_list(pages)

    @strawberry.field
    async def manga(
        self, info: Info
    ) -> Annotated["MangaType", strawberry.lazy("gql.modules.manga.types")]:
        from gql.modules.manga.types import MangaType

        manga = await info.context.loaders.manga_by_branch_id.load(
            self._instance.branch_id
        )
        return MangaType.from_orm(cast(Manga, manga))
