from typing import Annotated

import strawberry
from aioinject import Inject
from aioinject.ext.strawberry import inject

from core.chapters.services import MangaChapterService
from db.models import Language

from ...types import LanguageType
from .types import MangaChapterType


@strawberry.type
class MangaChapterQuery:
    @strawberry.field
    @inject
    async def manga_chapter_get(
        self,
        id: strawberry.ID,
        chapter_service: Annotated[MangaChapterService, Inject],
    ) -> MangaChapterType | None:
        chapter = await chapter_service.get(id=id)
        return MangaChapterType.from_orm_optional(chapter)

    @strawberry.field
    @inject
    async def manga_chapters_recent(
        self,
        service: Annotated[MangaChapterService, Inject],
        languages: list[LanguageType] | None = None,
    ) -> list[MangaChapterType]:
        languages = [Language[lang.value] for lang in languages] if languages else None
        chapters = await service.recent(
            languages=languages,
        )
        return MangaChapterType.from_orm_list(chapters)
