import strawberry


@strawberry.input(
    name="MangaChapterCreateInput",
)
class ChapterCreateInput:
    branch_id: strawberry.ID
    number: str
    title: str | None = None
    volume: int | None = None
