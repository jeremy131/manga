import collections
from typing import Iterable
from uuid import UUID

from common.utils import filter_uuids
from sqlalchemy import select

from db.base import async_sessionmaker
from db.models import MangaChapter


async def load_manga_chapters_by_manga_id(
    manga_ids: list[UUID | str],
) -> list[list[MangaChapter]]:
    stmt = select(MangaChapter).where(
        MangaChapter.manga_id.in_(filter_uuids(manga_ids))
    )
    async with async_sessionmaker() as session:
        chapters: Iterable[MangaChapter] = await session.scalars(stmt)

    chapters_by_manga_id = collections.defaultdict(list)
    for chapter in chapters:
        chapters_by_manga_id[str(chapter.manga_id)].append(chapter)

    return [chapters_by_manga_id.get(str(manga_id), []) for manga_id in manga_ids]
