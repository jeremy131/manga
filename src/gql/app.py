from starlette.requests import Request
from starlette.responses import Response
from starlette.websockets import WebSocket
from strawberry.asgi import GraphQL
from strawberry.dataloader import DataLoader

from .context import Context, Loaders
from .modules.chapters.loaders import load_manga_chapters_by_manga_id
from .modules.manga.dataloaders import (
    load_manga_by_branch_id,
    load_manga_by_id,
    load_manga_infos_by_manga_id,
)
from .modules.pages.dataloaders import load_chapter_pages
from .modules.tags.dataloaders import load_manga_tags
from .schema import schema


class GraphQLApp(GraphQL):
    async def get_context(
        self,
        request: Request | WebSocket,
        response: Response | None = None,
    ) -> Context:
        return Context(
            request=request,
            response=response,
            loaders=Loaders(
                manga_by_id=DataLoader(load_manga_by_id),
                manga_by_branch_id=DataLoader(load_manga_by_branch_id),
                manga_tags=DataLoader(load_manga_tags),
                manga_info_by_manga_id=DataLoader(load_manga_infos_by_manga_id),
                manga_chapters_by_manga_id=DataLoader(load_manga_chapters_by_manga_id),
                pages_by_chapter_id=DataLoader(load_chapter_pages),
            ),
        )


def create_gql_app() -> GraphQL:
    app = GraphQLApp(
        schema=schema,
    )

    return app
