import math
from typing import Generic, Type, TypeVar

import pydantic
import strawberry
from common.utils import OrmMixin
from pydantic import BaseModel
from sqlalchemy import func, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.sql import Select

TNode = TypeVar("TNode")
TEdge = TypeVar("TEdge")
T = TypeVar("T")
TModel = TypeVar("TModel")
TType = TypeVar("TType")


# @strawberry.type(name="PageInfo")
# class PageInfo:
#     has_next_page: bool = False
#     has_previous_page: bool = False
#     start_cursor: str = ""
#     end_cursor: str = ""
#

# @strawberry.type(name="Connection")
# class Connection(Generic[TEdge]):
#     edges: list[TEdge]
#     page_info: PageInfo


@strawberry.type(name="Edge")
class Edge(Generic[TNode]):
    node: TNode
    cursor: str = ""


class PagePaginationPydantic(BaseModel):
    page: int = pydantic.Field(default=1, ge=1)
    page_size: int = pydantic.Field(default=100, ge=1, le=1_000)


@strawberry.experimental.pydantic.input(PagePaginationPydantic)
class PagePaginationInput:
    page: strawberry.auto
    page_size: strawberry.auto


@strawberry.type
class PagePaginationInfo:
    current_page: int
    page_size: int
    total_items: int
    has_next_page: bool
    has_previous_page: bool

    @strawberry.field
    def total_pages(self) -> int:
        return math.ceil(self.total_items / self.page_size)


@strawberry.type
class PagePaginationResult(Generic[T]):
    items: list[T]
    page_info: PagePaginationInfo


async def page_paginate(
    query: Select,
    session: AsyncSession,
    type_: Type[OrmMixin[TModel, TType]],
    page: int = 1,
    page_size: int = 100,
) -> PagePaginationResult[TType]:
    items_query = query.limit(page_size).offset((page - 1) * page_size)
    items: list[TModel] = (await session.scalars(items_query)).all()
    total_items = await session.scalar(
        select(func.count()).select_from(query.order_by(None).subquery())
    )
    page_info = PagePaginationInfo(
        current_page=page,
        page_size=page_size,
        total_items=total_items,
        has_next_page=page * page_size < total_items,
        has_previous_page=page > 1,
    )
    return PagePaginationResult(
        items=type_.from_orm_list(items),
        page_info=page_info,
    )
