from __future__ import annotations

import dataclasses
import uuid
from functools import cached_property

from common.auth.models import AuthTokenPayload
from common.settings import get_settings
from common.web.auth import AuthenticationError, authenticate_user
from starlette.requests import Request
from starlette.responses import Response
from starlette.websockets import WebSocket
from strawberry.dataloader import DataLoader
from strawberry.types import Info as StrawberryInfo

from db.models import Manga, MangaChapter, MangaInfo, MangaPage, MangaTag
from settings import AuthSettings


@dataclasses.dataclass
class Loaders:
    manga_by_id: DataLoader[uuid.UUID | str, Manga | None]
    manga_by_branch_id: DataLoader[uuid.UUID, Manga | None]
    manga_tags: DataLoader[uuid.UUID | str, list[MangaTag]]
    manga_info_by_manga_id: DataLoader[uuid.UUID | str, list[MangaInfo]]
    manga_chapters_by_manga_id: DataLoader[uuid.UUID | str, list[MangaChapter]]
    pages_by_chapter_id: DataLoader[uuid.UUID | str, list[MangaPage]]


@dataclasses.dataclass
class Context:
    request: Request | WebSocket
    response: Response | None
    loaders: Loaders

    @cached_property
    def user(self) -> AuthTokenPayload:
        if not isinstance(self.request, Request):
            raise AuthenticationError

        settings = get_settings(AuthSettings)
        return authenticate_user(
            token=self.request.headers.get("Authorization", ""),
            public_key=settings.jwt_public_key,
            algorithm=settings.jwt_algorithm,
        )

    @cached_property
    def maybe_user(self) -> AuthTokenPayload | None:
        try:
            return self.user
        except AuthenticationError:
            return None


class Info(StrawberryInfo[Context, None]):
    pass
