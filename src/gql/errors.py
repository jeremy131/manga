from typing import TypeVar

import strawberry.federation

T = TypeVar("T")


@strawberry.federation.interface(name="Error")
class ErrorType:
    message: str


@strawberry.federation.type(name="PermissionError")
class PermissionErrorType(ErrorType):
    message: str = "Not Authorized"


@strawberry.federation.type(name="UnauthenticatedError")
class UnauthenticatedError(ErrorType):
    message: str = "Unauthenticated"


@strawberry.federation.type(name="EntityAlreadyExistsError")
class EntityAlreadyExistsError(ErrorType):
    message: str = "Entity Already Exists"
