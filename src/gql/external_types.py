import strawberry.federation


@strawberry.federation.type(
    keys=["id"],
    name="User",
)
class UserType:
    id: strawberry.ID
