import strawberry.federation
from aioinject.ext.strawberry import make_container_ext
from strawberry.extensions import ParserCache, ValidationCache
from strawberry.tools import merge_types

from di import create_container
from gql.errors import PermissionErrorType
from gql.modules.admin_manga.query import AdminMangaQuery
from gql.modules.chapters.mutation import MangaChapterMutation
from gql.modules.chapters.query import MangaChapterQuery
from gql.modules.groups.mutation import MangaGroupMutation
from gql.modules.groups.query import MangaGroupQuery
from gql.modules.manga.mutation import MangaMutation
from gql.modules.manga.query import MangaQuery
from gql.modules.tags.mutations import MangaTagMutation
from gql.modules.tags.query import MangaTagQuery

Query = merge_types(
    name="Query",
    types=(
        MangaQuery,
        MangaChapterQuery,
        AdminMangaQuery,
        MangaTagQuery,
        MangaGroupQuery,
    ),
)

Mutation = merge_types(
    name="Mutation",
    types=(
        MangaMutation,
        MangaChapterMutation,
        MangaTagMutation,
        MangaGroupMutation,
    ),
)

schema = strawberry.federation.Schema(  # type: ignore
    query=Query,
    mutation=Mutation,
    extensions=[
        make_container_ext(create_container()),
        ParserCache(),
        ValidationCache(),
    ],
    enable_federation_2=True,
    types=[PermissionErrorType],
)
