import faust
from common.settings import get_settings
from faust import StreamT

from db.base import async_sessionmaker
from db.models import User
from settings import FaustSettings

from .records import UserRecord

settings = get_settings(FaustSettings)
app = faust.App(
    id="manga",
    broker=f"kafka://{settings.kafka_host}",
    sasl_mechanism="PLAIN",
)

user_topic = app.topic("user", value_type=UserRecord)


@app.agent(user_topic)
async def hello(users: StreamT[UserRecord]) -> None:
    async for user_event in users:
        async with async_sessionmaker.begin() as session:
            user: User = await session.get(User, user_event.id) or User(
                id=user_event.id
            )
            user.username = user_event.username
            user.created_at = user_event.created_at
            session.add(user)


if __name__ == "__main__":
    app.main()
