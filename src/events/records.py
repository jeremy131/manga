import datetime
import uuid

import faust


class UserRecord(faust.Record, coerce=True):
    id: uuid.UUID
    username: str
    created_at: datetime.datetime
    is_deleted: bool = False
