FROM python:3.10-slim as build

RUN pip install pdm
COPY ./pyproject.toml ./pdm.lock ./
RUN pdm export --prod -f requirements -o requirements.txt

FROM python:3.10-slim
ENV PYTHONPATH=$PYTHONPATH:/app/src \
    PATH=$PATH:/home/app/.local/bin
RUN apt-get update && apt-get install -y curl

RUN adduser -u 1000 app
USER app

WORKDIR /app

COPY --from=build ./requirements.txt .
RUN pip install -r requirements.txt

COPY ./src ./src
COPY alembic.ini ./
ENTRYPOINT ["uvicorn", "app:create_app", "--factory", "--loop", "uvloop", "--host", "0.0.0.0", "--port", "8000"]
