import itertools
import uuid
from operator import attrgetter

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from core.chapters.services import MangaChapterService
from core.groups.commands import MangaGroupCreateCommand
from core.groups.services import GroupService
from core.info.services import MangaInfoService
from core.manga.dto import MangaCreateDto
from core.manga.services import MangaSearchService, MangaService
from core.tags.services import MangaTagService
from db.models import (
    Language,
    Manga,
    MangaBranch,
    MangaChapter,
    MangaGroup,
    MangaInfo,
    MangaPage,
)


@pytest.fixture
def manga_service(session: AsyncSession) -> MangaService:
    return MangaService(session=session)


@pytest.fixture
def manga_search_service(session: AsyncSession) -> MangaSearchService:
    return MangaSearchService(session=session)


@pytest.fixture
def chapter_service(session: AsyncSession) -> MangaChapterService:
    return MangaChapterService(session=session)


@pytest.fixture
def manga_info_service(session: AsyncSession) -> MangaInfoService:
    return MangaInfoService(session=session)


@pytest.fixture
def group_service(session: AsyncSession) -> GroupService:
    return GroupService(session=session)


@pytest.fixture
def manga_tag_service(session: AsyncSession) -> MangaTagService:
    return MangaTagService(session=session)


@pytest.fixture
async def manga(manga_service: MangaService) -> Manga:
    return await manga_service.create(
        dto=MangaCreateDto(title=str(uuid.uuid4())),
    )


@pytest.fixture
def manga_group_create_command(group_service: GroupService) -> MangaGroupCreateCommand:
    return MangaGroupCreateCommand(
        group_service=group_service,
    )


@pytest.fixture
async def group(session: AsyncSession) -> MangaGroup:
    group = MangaGroup(
        created_by_id=uuid.uuid4(),
        title="Manga Group",
    )
    session.add(group)
    await session.flush()
    await session.refresh(group)
    return group


@pytest.fixture
async def branch(
    manga: Manga,
    group: MangaGroup,
    session: AsyncSession,
) -> MangaBranch:
    branch = MangaBranch(
        manga_id=manga.id,
        group_id=group.id,
        language=Language.eng,
    )
    session.add(branch)
    await session.flush()
    await session.refresh(branch)
    return branch


@pytest.fixture
async def chapter(
    branch: MangaBranch,
    session: AsyncSession,
) -> MangaChapter:
    chapter = MangaChapter(
        branch_id=branch.id,
        title="Title",
        created_by_id=uuid.uuid4(),
        volume=1,
        number="1",
    )
    session.add(chapter)
    await session.flush()
    await session.refresh(chapter)
    return chapter


@pytest.fixture
async def chapter_pages(
    chapter: MangaChapter,
    session: AsyncSession,
) -> list[MangaPage]:
    pages = [MangaPage(chapter=chapter, number=i, image_url="") for i in range(30)]
    session.add_all(pages)
    await session.flush()
    for page in pages:
        await session.refresh(page)
    return pages


@pytest.fixture
async def infos(
    session: AsyncSession,
    manga: Manga,
) -> list[MangaInfo]:
    infos = [
        MangaInfo(
            manga=manga,
            language=language,
            title=f"Title {i}",
            description=f"Description {i}",
        )
        for i, language in zip(
            range(4),
            itertools.cycle([Language.eng, Language.rus]),
        )
    ]
    session.add_all(infos)
    await session.flush()
    for info in infos:
        await session.refresh(info)

    infos.sort(key=attrgetter("id"))
    return infos
