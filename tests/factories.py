import uuid
from collections import defaultdict
from typing import Any, Awaitable, Callable

import pytest
import slugify
from faker import Faker
from sqlalchemy.ext.asyncio import AsyncSession

from db.models import Manga, MangaBranch, MangaChapter


@pytest.fixture
def manga_factory(
    fake: Faker, session: AsyncSession
) -> Callable[..., Awaitable[Manga]]:
    async def factory() -> Manga:
        title = fake.sentence()
        manga = Manga(
            title=title,
            title_slug=slugify.slugify(title),
            created_at=fake.date_time(),
        )
        session.add(manga)
        await session.flush()
        await session.refresh(manga)
        return manga

    return factory


@pytest.fixture
def chapter_factory(
    fake: Faker, session: AsyncSession
) -> Callable[..., Awaitable[MangaChapter]]:
    chapter_numbers: dict[uuid.UUID, tuple[int, int]] = defaultdict(lambda: (1, 0))

    async def factory(branch: MangaBranch, **kwargs: Any) -> MangaChapter:
        if not (number := kwargs.pop("number", None)):
            major, minor = chapter_numbers[branch.id]
            number = f"{major}.{minor}" if minor else str(major)
            minor += 1
            if minor > 3:
                major += 1
                minor = 0
            chapter_numbers[branch.id] = (major, minor)

        chapter = MangaChapter(
            title=fake.sentence(),
            created_at=fake.date_time(),
            number=number,
            branch=branch,
            **kwargs,
        )
        session.add(chapter)
        await session.flush()
        await session.refresh(chapter)
        return chapter

    return factory
