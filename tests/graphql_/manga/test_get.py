import uuid

import httpx

from db.models import Manga

QUERY = """
query Query($id: ID = null, $titleSlug: String = null) {
    manga: mangaGet(id: $id, titleSlug: $titleSlug) {
        id
        title
        titleSlug
        createdAt
        updatedAt
    }
}
"""


async def test_not_found(
    http_client: httpx.AsyncClient,
) -> None:
    response = await http_client.post(
        "/graphql/",
        json={
            "query": QUERY,
            "variables": {
                "id": str(uuid.uuid4()),
            },
        },
    )
    response_json = response.json()
    assert "errors" not in response_json
    data = response_json["data"]
    assert data["manga"] is None


async def test_manga(
    manga: Manga,
    http_client: httpx.AsyncClient,
) -> None:
    variables_set = [
        {"id": str(manga.id)},
        {"titleSlug": manga.title_slug},
    ]
    for variables in variables_set:
        response = await http_client.post(
            "/graphql/", json={"query": QUERY, "variables": variables}
        )
        response_json = response.json()
        assert "errors" not in response_json
        assert response_json["data"] == {
            "manga": {
                "id": str(manga.id),
                "title": manga.title,
                "titleSlug": manga.title_slug,
                "createdAt": manga.created_at.isoformat(),
                "updatedAt": manga.updated_at.isoformat(),
            }
        }
