from test.graphql_client import GraphQLClient

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from db.models import Manga

QUERY = """
mutation Mutation($input: MangaCreateInput!) {
    manga: mangaCreate(input: $input) {
        manga {
            id
            title
            titleSlug
        }
        errors {
            __typename
        }
    }
}
"""


async def test_validation(
    graphql_client: GraphQLClient,
) -> None:
    title = "a" * 81
    response = await graphql_client(query=QUERY, variables={"input": {"title": title}})
    assert "errors" not in response
    data = response["data"]
    assert data["manga"]["manga"] is None
    assert data["manga"]["errors"][0]["__typename"] == "ValidationErrors"


async def test_create(
    session: AsyncSession,
    graphql_client: GraphQLClient,
) -> None:
    title = "Test Manga Title"
    response = await graphql_client(query=QUERY, variables={"input": {"title": title}})
    assert "errors" not in response

    manga = await session.scalar(select(Manga))

    data = response["data"]
    assert data["manga"]["manga"] == {
        "id": str(manga.id),
        "title": title,
        "titleSlug": "test-manga-title",
    }
    assert data["manga"]["errors"] == []
