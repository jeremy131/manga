import operator
from typing import Awaitable, Callable

import httpx

from db.models import Manga

QUERY = """
query SearchManga($input: MangaSearchInput! = {}, $pagination: PagePaginationInput! = {}) {
    search: mangaSearch(input: $input, pagination: $pagination) {
        pageInfo {
            currentPage
            hasNextPage
            hasPreviousPage
            pageSize
            totalItems
            totalPages
        }
        items {
            id
        }
    }
}
"""


async def test_search(
    http_client: httpx.AsyncClient,
    manga_factory: Callable[..., Awaitable[Manga]],
) -> None:
    manga_list = [await manga_factory() for _ in range(50)]
    manga_list.sort(key=operator.attrgetter("created_at"), reverse=True)

    for page in range(1, 50 // 5 + 1):
        response = await http_client.post(
            "/graphql/",
            json={
                "query": QUERY,
                "variables": {
                    "pagination": {
                        "pageSize": 10,
                        "page": page,
                    }
                },
            },
        )
        response_json = response.json()
        assert "errors" not in response_json
        assert response_json["data"] == {
            "search": {
                "pageInfo": {
                    "currentPage": page,
                    "hasNextPage": page < 50 // 10,
                    "hasPreviousPage": page > 1,
                    "pageSize": 10,
                    "totalItems": len(manga_list),
                    "totalPages": 5,
                },
                "items": [
                    {"id": str(manga.id)}
                    for manga in manga_list[(page - 1) * 10 : page * 10]
                ],
            }
        }
