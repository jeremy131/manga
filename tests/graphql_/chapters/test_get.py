from httpx import AsyncClient

from db.models import MangaChapter, MangaPage

QUERY = """
query ChapterGet($id: ID!) {
    chapter: mangaChapterGet(id: $id) {
        id
        number
        title
        volume
        pages {
            id
            number
            imageUrl
        }
        createdBy {
            __typename
            id
        }
    }
}
"""


async def test_get(
    chapter: MangaChapter,
    chapter_pages: list[MangaPage],
    http_client: AsyncClient,
) -> None:
    response = await http_client.post(
        "/graphql/",
        json={
            "query": QUERY,
            "variables": {
                "id": str(chapter.id),
            },
        },
    )
    response_json = response.json()
    assert "errors" not in response_json
    assert response_json["data"]["chapter"] == {
        "id": str(chapter.id),
        "number": chapter.number,
        "title": chapter.title,
        "volume": chapter.volume,
        "createdBy": {
            "__typename": "User",
            "id": str(chapter.created_by_id),
        },
        "pages": [
            {
                "id": str(page.id),
                "number": page.number,
                "imageUrl": page.image_url,
            }
            for page in chapter_pages
        ],
    }
