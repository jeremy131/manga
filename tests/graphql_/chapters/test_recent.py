import operator
import uuid
from typing import Awaitable, Callable

import httpx

from db.models import MangaBranch, MangaChapter

QUERY = """
query RecentChapters($languages: [Language!]! = []) {
    chapters: mangaChaptersRecent(languages: $languages) {
        id
    }
}
"""


async def test_recent_chapters_empty(
    http_client: httpx.AsyncClient,
) -> None:
    response = await http_client.post(
        "/graphql/",
        json={
            "query": QUERY,
        },
    )
    response_json = response.json()
    assert "errors" not in response_json
    assert response_json["data"]["chapters"] == []


async def test_recent_chapters(
    branch: MangaBranch,
    chapter_factory: Callable[..., Awaitable[MangaChapter]],
    http_client: httpx.AsyncClient,
) -> None:
    chapters = [
        await chapter_factory(
            branch=branch,
            number=str(i),
            created_by_id=uuid.uuid4(),
        )
        for i in range(50)
    ]

    response = await http_client.post(
        "/graphql/",
        json={
            "query": QUERY,
        },
    )
    response_json = response.json()
    assert "errors" not in response_json
    assert response_json["data"]["chapters"] == [
        {"id": str(chapter.id)}
        for chapter in sorted(
            chapters, key=operator.attrgetter("created_at"), reverse=True
        )
    ]
