from test.graphql_client import GraphQLClient

from db.models import Manga, MangaChapter

QUERY = """
query MangaChapterGet($id: ID!) {
    chapter: mangaChapterGet(id: $id) {
        id
        manga {
            id
        }
    }
}
"""


async def test_chapter_manga(
    chapter: MangaChapter, manga: Manga, graphql_client: GraphQLClient
) -> None:
    response = await graphql_client(query=QUERY, variables={"id": str(chapter.id)})
    assert response["data"]["chapter"] == {
        "id": str(chapter.id),
        "manga": {"id": str(manga.id)},
    }
