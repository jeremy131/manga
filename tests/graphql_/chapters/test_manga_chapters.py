import uuid
from test.graphql_client import GraphQLClient
from typing import Awaitable, Callable

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from db.models import Manga, MangaBranch, MangaChapter

QUERY = """
query GetMangaChapters(
    $mangaId: ID!,
    $page: Int! = 1,
    $pageSize: Int! = 100,
) {
    manga: mangaGet(id: $mangaId) {
        id
        chapters(page: $page, pageSize: $pageSize) {
            items {
                id
            }
        }
    }
}
"""


@pytest.mark.parametrize("chapter_amount", (0, 50))
async def test_manga_chapters(
    chapter_amount: int,
    chapter_factory: Callable[..., Awaitable[MangaChapter]],
    manga: Manga,
    branch: MangaBranch,
    session: AsyncSession,
    graphql_client: GraphQLClient,
) -> None:
    chapters = [
        await chapter_factory(branch=branch, created_by_id=uuid.uuid4())
        for _ in range(chapter_amount)
    ]
    chapters.sort(
        key=lambda chapter: tuple(int(i) for i in chapter.number.split(".")),
        reverse=True,
    )

    response = await graphql_client(query=QUERY, variables={"mangaId": str(manga.id)})
    data = response["data"]
    assert data["manga"] == {
        "id": str(manga.id),
        "chapters": {"items": [{"id": str(chapter.id)} for chapter in chapters]},
    }
