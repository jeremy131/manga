from db.models import Language
from gql.types import LanguageType


def test_language_enum_compat() -> None:
    for lang in Language:
        assert LanguageType(lang.name)

    for lang in LanguageType:
        assert Language(lang.value)
        assert Language[lang.value]
