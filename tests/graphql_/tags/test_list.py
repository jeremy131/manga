import operator

import httpx
import pytest
from faker import Faker

from core.tags.dto import MangaTagCreateDto
from core.tags.services import MangaTagService

QUERY = """
query ListTags {
    tags: mangaTags {
        id
        name
        nameSlug
    }
}
"""


@pytest.mark.parametrize(
    "tags_count",
    (0, 50),
)
async def test_get_tags(
    tags_count: int,
    fake: Faker,
    manga_tag_service: MangaTagService,
    http_client: httpx.AsyncClient,
) -> None:
    tags = [
        await manga_tag_service.create(
            dto=MangaTagCreateDto(name=fake.text(max_nb_chars=40))
        )
        for _ in range(tags_count)
    ]
    response = await http_client.post("/graphql/", json={"query": QUERY})
    response_json = response.json()
    assert "errors" not in response_json
    assert response_json["data"]["tags"] == [
        {
            "id": str(tag.id),
            "name": tag.name,
            "nameSlug": tag.name_slug,
        }
        for tag in sorted(tags, key=operator.attrgetter("name_slug"))
    ]
