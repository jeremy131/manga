from test.graphql_client import GraphQLClient

import pytest
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from core.tags.services import MangaTagService
from db.models import MangaTag

QUERY = """
mutation CreateTag($tag: MangaTagCreateInput!) {
    result: mangaTagCreate(input: $tag) {
        tag {
            id
            name
            nameSlug
        }
        errors {
            __typename
        }
    }
}
"""


async def test_validation(
    graphql_client: GraphQLClient,
) -> None:
    name = " " * 41
    response = await graphql_client(query=QUERY, variables={"tag": {"name": name}})
    assert "errors" not in response
    data = response["data"]
    assert data["result"]["tag"] is None
    assert data["result"]["errors"][0]["__typename"] == "ValidationErrors"


@pytest.mark.parametrize(
    "name,name_slug,new_name",
    (
        ("a", "a", "a"),
        ("Name", "name", "Name"),
        ("Name", "name", "name"),
    ),
)
async def test_duplicate_tag(
    name: str,
    name_slug: str,
    new_name: str,
    session: AsyncSession,
    graphql_client: GraphQLClient,
    manga_tag_service: MangaTagService,
) -> None:
    tag = MangaTag(name=name, name_slug=name_slug)
    session.add(tag)
    await session.flush()

    response = await graphql_client(
        query=QUERY,
        variables={"tag": {"name": new_name}},
    )
    assert "errors" not in response
    data = response["data"]
    assert data["result"]["tag"] is None
    assert data["result"]["errors"][0]["__typename"] == "EntityAlreadyExistsError"


async def test_create_tag(
    session: AsyncSession,
    graphql_client: GraphQLClient,
) -> None:
    response = await graphql_client(
        query=QUERY,
        variables={"tag": {"name": "Tag Name"}},
    )
    new_tag = await session.scalar(select(MangaTag))

    assert "errors" not in response
    data = response["data"]
    assert data["result"]["errors"] == []
    assert data["result"]["tag"] == {
        "id": str(new_tag.id),
        "name": "Tag Name",
        "nameSlug": "tag-name",
    }
