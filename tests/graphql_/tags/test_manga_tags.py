import operator

import httpx
from faker import Faker
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload

from core.tags.dto import MangaTagCreateDto
from core.tags.services import MangaTagService
from db.models import Manga

QUERY = """
query MangaTags($id: ID = null) {
    manga: mangaGet(id: $id) {
        id
        tags {
            id
            name
            nameSlug
        }
    }
}
"""


async def test_manga_tags(
    manga: Manga,
    session: AsyncSession,
    fake: Faker,
    manga_tag_service: MangaTagService,
    http_client: httpx.AsyncClient,
) -> None:
    tags = [
        await manga_tag_service.create(
            MangaTagCreateDto(name=fake.text(max_nb_chars=40))
        )
        for _ in range(10)
    ]
    manga = await session.scalar(
        select(Manga).where(Manga.id == manga.id).options(joinedload(Manga.tags))
    )
    manga.tags = tags
    await session.flush()

    response = await http_client.post(
        "/graphql/", json={"query": QUERY, "variables": {"id": str(manga.id)}}
    )
    response_json = response.json()
    assert "errors" not in response_json
    assert response_json["data"] == {
        "manga": {
            "id": str(manga.id),
            "tags": [
                {
                    "id": str(tag.id),
                    "name": tag.name,
                    "nameSlug": tag.name_slug,
                }
                for tag in sorted(tags, key=operator.attrgetter("name_slug"))
            ],
        }
    }
