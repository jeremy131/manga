import uuid
from test.graphql_client import GraphQLClient

from common.auth.models import AuthTokenPayload

from core.groups.dtos import MangaGroupCreateDto
from core.groups.services import GroupService

QUERY = """
mutation GroupCreate($input: MangaGroupCreateInput!) {
    group: mangaGroupCreate(input: $input) {
        result {
            id
            title
            createdBy {
                id
            }
        }
        errors {
            __typename
        }
    }
}
"""


async def test_not_authenticated(
    graphql_client: GraphQLClient,
) -> None:
    response = await graphql_client(QUERY, variables={"input": {"title": ""}})
    assert response["data"]["group"]["errors"] == [
        {"__typename": "UnauthenticatedError"}
    ]


async def test_validation(
    graphql_client: GraphQLClient,
    auth_token_str: str,
) -> None:
    response = await graphql_client(
        QUERY,
        variables={"input": {"title": ""}},
        headers={"Authorization": f"Bearer {auth_token_str}"},
    )
    assert response["data"]["group"]["errors"] == [{"__typename": "ValidationErrors"}]


async def test_duplicate(
    group_service: GroupService,
    graphql_client: GraphQLClient,
    auth_token_str: str,
) -> None:
    title = str(uuid.uuid4())
    await group_service.create(
        dto=MangaGroupCreateDto(title=title),
        created_by=uuid.uuid4(),
    )
    response = await graphql_client(
        QUERY,
        variables={"input": {"title": title}},
        headers={"Authorization": f"Bearer {auth_token_str}"},
    )
    assert response["data"]["group"]["errors"] == [
        {"__typename": "EntityAlreadyExistsError"}
    ]


async def test_create(
    group_service: GroupService,
    graphql_client: GraphQLClient,
    auth_token_str: str,
    auth_token: AuthTokenPayload,
) -> None:
    title = str(uuid.uuid4())
    response = await graphql_client(
        QUERY,
        variables={"input": {"title": title}},
        headers={"Authorization": f"Bearer {auth_token_str}"},
    )
    group = await group_service.get(title=title)
    assert group
    assert response["data"]["group"]["result"] == {
        "id": str(group.id),
        "title": title,
        "createdBy": {
            "id": auth_token.sub,
        },
    }
