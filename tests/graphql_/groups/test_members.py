from test.graphql_client import GraphQLClient

from db.models import MangaGroup, MangaGroupMembership

QUERY = """
query GetGroupMembers($id: ID!) {
    group: mangaGroupGet(id: $id) {
        id
        members {
            id
        }
    }
}
"""


async def test_empty_members(
    group: MangaGroup,
    graphql_client: GraphQLClient,
) -> None:
    response = await graphql_client(query=QUERY, variables={"id": str(group.id)})
    assert response["data"]["group"]["members"] == []


async def test_members(
    group: MangaGroup,
    group_memberships: list[MangaGroupMembership],
    graphql_client: GraphQLClient,
) -> None:
    response = await graphql_client(query=QUERY, variables={"id": str(group.id)})
    assert response["data"]["group"]["members"] == [
        {"id": str(membership.member_id)}
        for membership in sorted(group_memberships, key=lambda m: m.member_id)
    ]
