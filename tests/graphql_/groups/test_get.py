import uuid
from test.graphql_client import GraphQLClient

import pytest

from db.models import MangaGroup

QUERY = """
query MangaGroupGet($id: ID!) {
    group: mangaGroupGet(id: $id) {
        id
        title
        createdAt
        createdBy {
            id
        }
    }
}
"""


async def test_not_found(
    graphql_client: GraphQLClient,
) -> None:
    response = await graphql_client(query=QUERY, variables={"id": str(uuid.uuid4())})
    assert response["data"]["group"] is None


@pytest.mark.parametrize("id", ("Invalid-Id", ""))
async def test_invalid_id(
    id: str,
    graphql_client: GraphQLClient,
) -> None:
    response = await graphql_client(query=QUERY, variables={"id": id})
    assert response["data"]["group"] is None


async def test_get(
    group: MangaGroup,
    graphql_client: GraphQLClient,
) -> None:
    response = await graphql_client(
        query=QUERY,
        variables={"id": str(group.id)},
    )
    assert response["data"]["group"] == {
        "id": str(group.id),
        "title": group.title,
        "createdAt": group.created_at.isoformat(),
        "createdBy": {
            "id": str(group.created_by_id),
        },
    }
