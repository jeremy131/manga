import httpx

from db.models import Manga, MangaInfo
from gql.types import LanguageType

_QUERY = """
query ($id: ID!, $preferredLanguages: [Language!] = null) {
    manga: mangaGet(id: $id) {
        id
        infos(preferredLanguages: $preferredLanguages)  {
            id
            language
            title
            description
        }
    }
}
"""


async def test_empty_infos(
    manga: Manga,
    http_client: httpx.AsyncClient,
) -> None:
    response = await http_client.post(
        "/graphql/", json={"query": _QUERY, "variables": {"id": str(manga.id)}}
    )
    response_json = response.json()
    assert "errors" not in response_json
    data = response_json["data"]
    assert data["manga"] == {
        "id": str(manga.id),
        "infos": [],
    }


async def test_infos(
    manga: Manga,
    infos: list[MangaInfo],
    http_client: httpx.AsyncClient,
) -> None:
    response = await http_client.post(
        "/graphql/",
        json={
            "query": _QUERY,
            "variables": {
                "id": str(manga.id),
            },
        },
    )
    response_json = response.json()
    assert "errors" not in response_json
    data = response_json["data"]
    assert data["manga"] == {
        "id": str(manga.id),
        "infos": [
            {
                "id": f"{info.id}",
                "title": info.title,
                "description": info.description,
                "language": LanguageType(info.language.value).name,
            }
            for info in infos
        ],
    }


async def test_info_preference(
    infos: list[MangaInfo],
    manga: Manga,
    http_client: httpx.AsyncClient,
) -> None:
    preferred_languages = ["en", "ru"]
    response = await http_client.post(
        "/graphql/",
        json={
            "query": _QUERY,
            "variables": {
                "id": str(manga.id),
                "preferredLanguages": preferred_languages,
            },
        },
    )
    response_json = response.json()
    assert "errors" not in response_json
    data = response_json["data"]
    assert data["manga"] == {
        "id": str(manga.id),
        "infos": [
            {
                "id": f"{info.id}",
                "title": info.title,
                "description": info.description,
                "language": LanguageType(info.language.value).name,
            }
            for info in sorted(
                infos,
                key=lambda i: preferred_languages.index(
                    LanguageType(i.language.value).name
                ),
            )
        ],
    }


async def test_missing_language(
    infos: list[MangaInfo],
    manga: Manga,
    http_client: httpx.AsyncClient,
) -> None:
    preferred_language = LanguageType.de.name
    response = await http_client.post(
        "/graphql/",
        json={
            "query": _QUERY,
            "variables": {
                "id": str(manga.id),
                "preferredLanguages": [preferred_language],
            },
        },
    )
    response_json = response.json()
    assert "errors" not in response_json
    data = response_json["data"]
    assert data["manga"] == {
        "id": str(manga.id),
        "infos": [
            {
                "id": f"{info.id}",
                "title": info.title,
                "description": info.description,
                "language": LanguageType(info.language.name).name,
            }
            for info in infos
        ],
    }
