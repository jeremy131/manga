import uuid

from sqlalchemy.ext.asyncio import AsyncSession

from core.groups.services import GroupService
from db.models import MangaGroup


async def test_get_group_not_found(group_service: GroupService) -> None:
    assert await group_service.get(uuid.uuid4()) is None


async def test_empty(
    group: MangaGroup,
    group_service: GroupService,
) -> None:
    assert await group_service.get() is None


async def test_get_group(
    session: AsyncSession,
    group: MangaGroup,
    group_service: GroupService,
) -> None:
    assert await group_service.get(group.id) is group
