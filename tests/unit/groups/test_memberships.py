import uuid

from sqlalchemy.ext.asyncio import AsyncSession

from core.groups.services import GroupService
from db.models import MangaGroup, MangaGroupMembership, User


async def test_get_group_not_found(group_service: GroupService) -> None:
    assert await group_service.get(uuid.uuid4()) is None


async def test_group_membership_empty(
    group: MangaGroup,
    group_service: GroupService,
) -> None:
    assert await group_service.group_memberships(uuid.uuid4()) == []
    assert await group_service.group_memberships(group.id) == []


async def test_group_membership(
    session: AsyncSession,
    group: MangaGroup,
    group_memberships_with_user_replicas: list[tuple[MangaGroupMembership, User]],
    group_service: GroupService,
) -> None:
    memberships_with_users = sorted(
        [(m, u) for m, u in group_memberships_with_user_replicas if u],
        key=lambda v: v[1].username,
    )
    memberships_without_users = sorted(
        [(m, u) for m, u in group_memberships_with_user_replicas if u is None],
        key=lambda v: v[0].member_id,
    )
    expected = [
        membership
        for membership, _ in memberships_with_users + memberships_without_users
    ]
    assert await group_service.group_memberships(group.id) == expected
