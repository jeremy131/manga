import uuid

import pytest
from common.auth.models import AuthTokenPayload

from core.exceptions import EntityAlreadyExists
from core.groups.commands import MangaGroupCreateCommand
from core.groups.dtos import MangaGroupCreateDto
from core.groups.services import GroupService


async def test_duplicate(
    auth_token: AuthTokenPayload,
    group_service: GroupService,
    manga_group_create_command: MangaGroupCreateCommand,
) -> None:
    title = "Group Title"
    await group_service.create(
        dto=MangaGroupCreateDto(title=title),
        created_by=uuid.uuid4(),
    )
    with pytest.raises(EntityAlreadyExists):
        await manga_group_create_command.execute(
            dto=MangaGroupCreateDto(title=title),
            auth=auth_token,
        )


async def test_create(
    auth_token: AuthTokenPayload,
    group_service: GroupService,
    manga_group_create_command: MangaGroupCreateCommand,
) -> None:
    dto = MangaGroupCreateDto(
        title=str(uuid.uuid4()),
    )
    group = await manga_group_create_command.execute(dto, auth_token)
    assert group.title == dto.title
    assert group.created_by_id == uuid.UUID(auth_token.sub)

    assert await group_service.get(id=group.id, title=group.title) is group
