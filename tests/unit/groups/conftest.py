import datetime
import random
import uuid

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from db.models import MangaGroup, MangaGroupMembership, User


@pytest.fixture
async def group_memberships(
    session: AsyncSession,
    group: MangaGroup,
) -> list[MangaGroupMembership]:
    memberships = [
        MangaGroupMembership(group=group, member_id=uuid.uuid4()) for _ in range(10)
    ]
    session.add_all(memberships)
    await session.flush()
    for membership in memberships:
        await session.refresh(membership)

    return memberships


@pytest.fixture
async def group_memberships_with_user_replicas(
    session: AsyncSession,
    group_memberships: list[MangaGroupMembership],
) -> list[tuple[MangaGroupMembership, User | None]]:
    users = [
        User(
            id=membership.member_id,
            username=str(membership.member_id),
            created_at=datetime.datetime.now(),
        )
        for membership in random.sample(
            group_memberships, k=len(group_memberships) // 2
        )
    ]
    session.add_all(users)
    await session.flush()
    for user in users:
        await session.refresh(user)

    return [
        (
            membership,
            next((u for u in users if u.id == membership.member_id), None),
        )
        for membership in group_memberships
    ]
