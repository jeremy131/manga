import pytest

from core.info.dto import MangaInfoCreateDto
from core.info.services import MangaInfoService
from core.manga.dto import MangaCreateDto
from core.manga.services import MangaService
from db.models import Manga, MangaInfo
from db.models._language import Language


@pytest.fixture
async def manga(manga_service: MangaService) -> Manga:
    return await manga_service.create(dto=MangaCreateDto(title="Manga Title"))


@pytest.fixture
async def manga_infos(
    manga: Manga,
    manga_info_service: MangaInfoService,
) -> list[MangaInfo]:
    infos = [
        MangaInfoCreateDto(
            title="Manga Title",
            description="Manga Description",
            language=Language.eng,
            manga_id=manga.id,
        ),
    ]
    return [await manga_info_service.create(info) for info in infos]
