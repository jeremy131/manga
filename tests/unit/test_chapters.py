import operator
import uuid
from typing import Awaitable, Callable

from faker import Faker
from sqlalchemy.ext.asyncio import AsyncSession

from core.chapters.services import MangaChapterService
from db.models import Language, Manga, MangaBranch, MangaChapter


async def test_get_not_found(
    chapter_service: MangaChapterService,
) -> None:
    chapter_id = uuid.uuid4()

    assert await chapter_service.get(chapter_id) is None
    assert await chapter_service.get(str(chapter_id)) is None
    assert await chapter_service.get("not-a-uuid") is None


async def test_recent(
    branch: MangaBranch,
    fake: Faker,
    session: AsyncSession,
    chapter_service: MangaChapterService,
) -> None:
    chapters = [
        MangaChapter(
            branch=branch,
            title=f"{i}",
            volume=1,
            number=f"{i}",
            created_by_id=uuid.uuid4(),
            created_at=fake.date_time(),
        )
        for i in range(50)
    ]
    session.add_all(chapters)
    await session.flush()

    recent = await chapter_service.recent()
    assert recent == sorted(
        chapters, key=operator.attrgetter("created_at"), reverse=True
    )

    assert branch.language is Language.eng
    assert await chapter_service.recent(languages=[Language.deu]) == []


async def test_empty_manga_chapters(
    manga: Manga,
    chapter_service: MangaChapterService,
    session: AsyncSession,
) -> None:
    stmt = chapter_service.manga_chapters_query(manga.id)
    assert (await session.scalars(stmt)).all() == []


async def test_manga_chapters(
    branch: MangaBranch,
    chapter_factory: Callable[..., Awaitable[MangaChapter]],
    chapter_service: MangaChapterService,
    session: AsyncSession,
) -> None:
    chapters = [
        await chapter_factory(branch=branch, created_by_id=uuid.uuid4())
        for _ in range(100)
    ]
    chapters.sort(
        key=lambda chapter: tuple(int(i) for i in chapter.number.split(".")),
        reverse=True,
    )
    stmt = chapter_service.manga_chapters_query(branch.manga_id)
    assert (await session.scalars(stmt)).all() == chapters
