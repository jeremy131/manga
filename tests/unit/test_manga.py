import uuid

from core.manga.dto import MangaCreateDto
from core.manga.services import MangaService
from db.models import Manga


async def test_get_empty(manga_service: MangaService) -> None:
    assert await manga_service.get() is None


async def test_get_none(manga_service: MangaService) -> None:
    manga = await manga_service.get(entity_id=uuid.uuid4())
    assert manga is None


async def test_get_existing(manga_service: MangaService, manga: Manga) -> None:
    assert await manga_service.get(entity_id=manga.id) is manga


async def test_create(manga_service: MangaService) -> None:
    manga = await manga_service.create(dto=MangaCreateDto(title="Some Title"))
    assert isinstance(manga.id, uuid.UUID)
    assert manga.title == "Some Title"
    assert manga.title_slug == "some-title"
