from slugify import slugify
from sqlalchemy.ext.asyncio import AsyncSession

from core.info.services import MangaInfoService
from db.models import Manga, MangaInfo
from db.models._language import Language


async def test_create(session: AsyncSession) -> None:
    manga = Manga(title="Manga Title", title_slug=slugify("Manga Title"))
    info = MangaInfo(
        manga=manga,
        title="Manga Title",
        description="Manga Description",
        language=Language.eng,
    )
    session.add(manga)
    session.add(info)
    await session.flush()
    await session.refresh(info)


async def test_list(
    manga: Manga,
    manga_infos: list[MangaInfo],
    manga_info_service: MangaInfoService,
) -> None:
    infos = await manga_info_service.list(manga_ids=[manga.id])
    assert infos == manga_infos
