import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from core.manga.services import MangaSearchService
from db.models import Language, Manga, MangaInfo, MangaTag


@pytest.fixture
async def seed_data(
    session: AsyncSession,
) -> None:
    tag_one = MangaTag(name="One", name_slug="one")
    tag_two = MangaTag(name="Two", name_slug="two")

    manga = [
        Manga(
            title="1",
            title_slug="1",
            info=[
                MangaInfo(
                    title="keyword one",
                    description="",
                    language=Language.eng,
                )
            ],
            tags=[
                tag_one,
            ],
        ),
        Manga(
            title="2",
            title_slug="2",
            info=[
                MangaInfo(
                    title="keyword two",
                    description="",
                    language=Language.eng,
                )
            ],
            tags=[
                tag_two,
            ],
        ),
    ]
    session.add_all(manga)
    await session.flush()


async def test_empty(
    session: AsyncSession,
    manga_search_service: MangaSearchService,
) -> None:
    stmt = manga_search_service.search_query()
    results = (await session.scalars(stmt)).all()
    assert results == []


@pytest.mark.usefixtures("seed_data")
async def test_basic_fts(
    session: AsyncSession,
    manga_search_service: MangaSearchService,
) -> None:
    stmt = manga_search_service.search_query(search_term="one")
    result = (await session.scalars(stmt)).all()
    assert len(result) == 1
    assert result[0].title_slug == "1"


@pytest.mark.usefixtures("seed_data")
@pytest.mark.parametrize(
    "include,exclude,expected",
    (
        (["one"], [], "1"),
        (["two"], [], "2"),
        ([], ["two"], "1"),
        ([], ["one"], "2"),
        (["one"], ["one"], None),
        (["two"], ["two"], None),
        ([], ["one", "two"], None),
    ),
)
async def test_tag_search(
    include: list[str],
    exclude: list[str],
    expected: str | None,
    session: AsyncSession,
    manga_search_service: MangaSearchService,
) -> None:
    stmt = manga_search_service.search_query(tags_include=include, tags_exclude=exclude)
    result = (await session.scalars(stmt)).all()
    if not expected:
        assert result == []
        return

    assert len(result) == 1
    assert result[0].title_slug == expected
