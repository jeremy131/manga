import pytest

from core.exceptions import EntityAlreadyExists
from core.tags.dto import MangaTagCreateDto
from core.tags.services import MangaTagService


async def test_create(
    manga_tag_service: MangaTagService,
) -> None:
    dto = MangaTagCreateDto(name="Manga Tag Name")
    tag = await manga_tag_service.create(dto)
    assert tag.name == dto.name
    assert tag.name_slug == "manga-tag-name"


@pytest.mark.parametrize(
    "name1, name2",
    (
        ("duplicate name", "duplicate name"),
        ("duplicate name", "duplicate-name"),
        ("duplicate   name", "duplicate   name"),
        ("duplicate   name", "duplicate name"),
        ("duplicate-name", "duplicate---name"),
    ),
)
async def test_create_duplicate(
    name1: str,
    name2: str,
    manga_tag_service: MangaTagService,
) -> None:
    await manga_tag_service.create(dto=MangaTagCreateDto(name=name1))

    with pytest.raises(EntityAlreadyExists):
        await manga_tag_service.create(dto=MangaTagCreateDto(name=name2))
