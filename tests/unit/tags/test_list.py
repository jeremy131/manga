import operator

from faker import Faker

from core.tags.dto import MangaTagCreateDto
from core.tags.services import MangaTagService


async def test_empty(
    manga_tag_service: MangaTagService,
) -> None:
    assert await manga_tag_service.list() == []


async def test_base_case(
    fake: Faker,
    manga_tag_service: MangaTagService,
) -> None:
    tags = [
        await manga_tag_service.create(
            dto=MangaTagCreateDto(
                name=fake.text(max_nb_chars=40),
            )
        )
        for _ in range(20)
    ]

    assert await manga_tag_service.list() == sorted(
        tags, key=operator.attrgetter("name_slug")
    )
