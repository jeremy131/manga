import asyncio
import os
import sys
import time
import uuid
from asyncio import AbstractEventLoop
from test.graphql_client import GraphQLClient
from typing import AsyncIterable, Iterable

import dotenv
import faker
import httpx
import jwt
import pytest
from common.auth.models import AuthTokenPayload
from common.settings import get_settings
from fastapi import FastAPI

from settings import AuthSettings

dotenv.load_dotenv(".env")

pytest_plugins = [
    "conftest_db",
    "conftest_services",
    "factories",
]


@pytest.fixture(scope="session")
def fastapi_app() -> FastAPI:
    from app import create_app

    return create_app()


@pytest.fixture(scope="session")
async def http_client(fastapi_app: FastAPI) -> AsyncIterable[httpx.AsyncClient]:
    async with httpx.AsyncClient(
        app=fastapi_app,
        base_url="http://test",
    ) as client:
        yield client


@pytest.fixture(scope="session")
def event_loop() -> Iterable[AbstractEventLoop]:
    if sys.platform.startswith("win"):
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

    policy = asyncio.get_event_loop_policy()
    loop = policy.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def fake() -> faker.Faker:
    return faker.Faker()


@pytest.fixture(scope="session")
async def graphql_client(
    http_client: httpx.AsyncClient,
) -> GraphQLClient:

    return GraphQLClient(
        http_client=http_client,
        endpoint_path="/graphql/",
    )


@pytest.fixture(scope="session")
def auth_token() -> AuthTokenPayload:
    return AuthTokenPayload(
        sub=str(uuid.uuid4()),
        iat=int(time.time() - 60),
        exp=int(time.time() + 300),
        username=str(uuid.uuid4()),
    )


@pytest.fixture(scope="session")
def auth_token_str(auth_token: AuthTokenPayload) -> str:
    return jwt.encode(
        payload=auth_token.dict(),
        key=os.environ["AUTH_JWT_PRIVATE_KEY"],
        algorithm=get_settings(AuthSettings).jwt_algorithm,
    )
